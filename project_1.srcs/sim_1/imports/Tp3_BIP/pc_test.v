module pc_test;

	// Inputs
	reg clk;
	reg enable;
	reg reset;
	reg [3:0] pc_in; //d

	// Outputs
	wire [3:0] pc_out;//q

	// Instantiate the Unit Under Test (UUT)
	pc_register uut (
		.clk(clk), 
		.enable(enable), 
		.reset(reset), 
		.pc_in(pc_in), 
		.pc_out(pc_out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		enable = 1;
		reset = 0;
		pc_in =4'b1010;
		
		#25
		reset = 1;
		#10
		reset = 0;
//		#1
//		enable = 1;
//		#1
//		enable = 0;
	end
	
	always begin //clock de la placa 50Mhz
		#10 clk=~clk;
	end

endmodule

