module signal_extension_test;

	// Inputs
	reg [10:0] in_se;

	// Outputs
	wire [15:0] out_se;

	// Instantiate the Unit Under Test (UUT)
	signal_extension uut (
		.in_se(in_se), 
		.out_se(out_se)
	);

	initial begin
		// Initialize Inputs
		in_se = 0;
		#20
		in_se = 500;
		#20
		in_se = -300;
	end
      
endmodule

