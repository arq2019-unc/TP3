module muxA_test;

	// Inputs
	reg [1:0] selectA;
	reg [15:0] op;
	reg [15:0] se;
	reg [15:0] dm;

	// Outputs
	wire [15:0] muxA_out;

	// Instantiate the Unit Under Test (UUT)
	muxA uut (
		.selectA(selectA), 
		.op(op), 
		.se(se), 
		.dm(dm), 
		.muxA_out(muxA_out)
	);

	initial begin
		// Initialize Inputs
		selectA = 2;
		op = 3;
		se = 2;
		dm = 1;
		#100;
		selectA = 1;
		#100;
		selectA = 0;
		#100;
		selectA = 3;
		// Wait 100 ns for global reset to finish
	end
      
endmodule

