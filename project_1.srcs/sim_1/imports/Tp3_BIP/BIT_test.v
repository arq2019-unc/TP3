module BIT_test;

	// Inputs
	reg clk;
	reg reset_pc;

	// Outputs
	wire [15:0] acc_out;
	wire [15:0] pm_out;
	wire [3:0] pm_addr;
	wire [15:0] dm_out;
	wire [10:0] dm_addr;
	wire rd_enable;
	wire wr_enable;
	wire enable_pc;
	wire [7:0] count;

	// Instantiate the Unit Under Test (UUT)
	BIP uut (
		.clk(clk), 
		.reset_pc(reset_pc), 
		.acc_out(acc_out), 
		.pm_out(pm_out), 
		.pm_addr(pm_addr), 
		.dm_out(dm_out), 
		.dm_addr(dm_addr), 
		.rd_enable(rd_enable), 
		.wr_enable(wr_enable), 
		.enable_pc(enable_pc), 
		.count(count)
	);
   
	initial begin
		clk = 1;
		reset_pc = 0;
	end
	
   always begin //clock de la placa 50Mhz
		#10 clk = ~clk;
	end
   
endmodule

