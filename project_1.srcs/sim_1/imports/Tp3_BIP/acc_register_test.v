module acc_register_test;

	// Inputs
	reg clk;
	reg enable;
	reg [15:0] acc_in;

	// Outputs
	wire [15:0] acc_out;

	// Instantiate the Unit Under Test (UUT)
	acc_register uut (
		.clk(clk), 
		.enable(enable), 
		.acc_in(acc_in), 
		.acc_out(acc_out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		enable = 0;
		acc_in = 0;
		#20
		acc_in = 30;
		enable = 1;
		#27
		enable = 0;
	end
   
	always begin //clock de la placa 50Mhz
		#10 clk=~clk;
	end
  
endmodule

