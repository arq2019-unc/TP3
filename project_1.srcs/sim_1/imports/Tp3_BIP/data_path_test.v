module data_path_test;

	// Inputs
	reg clk;
	reg [1:0] selA;
	reg selB;
	reg wrACC;
	reg op;
	reg [10:0] operand_in;
	reg [15:0] dm_out;

	// Outputs
	wire [10:0] operand_out;
	wire [15:0] dm_in;

	// Instantiate the Unit Under Test (UUT)
	datapath uut (
		.clk(clk), 
		.selA(selA), 
		.selB(selB), 
		.wrACC(wrACC), 
		.op(op), 
		.operand_in(operand_in), 
		.dm_out(dm_out), 
		.operand_out(operand_out), 
		.dm_in(dm_in)
	);

	initial begin
		// Initialize Inputs
		clk = 0;		
		
		//LDI 10
		selA = 1;
		selB = 0;
		wrACC = 1;
		op = 0;
		operand_in = 10;
		dm_out = 0;
    end
    
	always begin //clock de la placa 50Mhz
		#10 clk=~clk;
	end     
      
endmodule

