module contador_clk_test;

	// Inputs
	reg clk;
	reg reset;
	reg enable;

	// Outputs
	wire [7:0] count;

	// Instantiate the Unit Under Test (UUT)
	contador_clk uut (
		.clk(clk), 
		.reset(reset), 
		.enable(enable), 
		.count(count)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		enable = 1;
		#120
			enable = 0;
		#20
			reset = 1;
		#35
			reset = 0;
			enable = 1;
		#120
			enable = 0;

	end
	
    always begin //clock de la placa 50Mhz
		#10 clk = ~clk;
	end  
	
endmodule

