module control_test;

	// Inputs
	reg clk;
	reg reset_pc;
	reg [15:0] pm_data;

	// Outputs
	wire [1:0] selA;
	wire selB;
	wire wrACC;
	wire op;
	wire wrRAM;
	wire rdRAM;
	wire [3:0] pc_in; //d
	wire [3:0] pc_out;//q

	// Instantiate the Unit Under Test (UUT)
	control uut (
		.selA(selA), 
		.selB(selB), 
		.wrACC(wrACC), 
		.op(op), 
		.wrRAM(wrRAM), 
		.rdRAM(rdRAM), 
		.pm_data(pm_data),
		.clk(clk), 
		.reset_pc(reset_pc), 
		.pc_in(pc_in), 
		.pc_out(pc_out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		pm_data=0;
		reset_pc = 0;
		#200
		reset_pc = 1;
		#60
		reset_pc = 0;
	end
  
	always begin //clock de la placa 50Mhz
		#10 clk=~clk;
	end
	
endmodule

