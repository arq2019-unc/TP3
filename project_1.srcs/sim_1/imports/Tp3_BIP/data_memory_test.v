module data_memory_test;

	// Inputs
	reg [11:0] addr;
	reg wr_en;
	reg rd_en;
	reg [15:0] in_data;

	// Outputs
	wire [15:0] out_data;

	// Instantiate the Unit Under Test (UUT)
	data_memory uut (
		.addr(addr), 
		.wr_en(wr_en), 
		.rd_en(rd_en), 
		.in_data(in_data), 
		.out_data(out_data)
	);

	initial begin
		// Initialize Inputs
		addr = 10;
		wr_en = 0;
		rd_en = 0;
		in_data = 0;
		#10
		addr = 0;
		wr_en = 1;
		in_data = 10;
		rd_en = 0;
		#20
		in_data = 10;
		wr_en = 0;
		addr = 20;
		rd_en = 0;
		#20
		in_data = 20;
		wr_en = 1;
		addr = 1;
		rd_en = 0;
		#20
		in_data = 20;
		wr_en = 0;
		addr = 0;
		rd_en = 1;		
	end
      
endmodule

