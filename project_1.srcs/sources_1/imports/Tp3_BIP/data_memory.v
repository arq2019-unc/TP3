module data_memory
#(
	parameter B = 16	//number of bits
)
(
	input wire [10:0] addr,
	input wire wr_en,
	input wire rd_en,
	input wire signed [B-1:0] in_data,
	output reg signed [B-1:0] out_data
);

reg[B-1:0]array_reg[3:0];

//write operation
always @(*)
	if(wr_en)
		array_reg[addr[3:0]]=in_data;

//read operation
always @(*)
	if(rd_en)
		out_data=array_reg[addr[3:0]];

endmodule
