module program_memory
#(
	parameter 	B = 16,	//number of bits
				W = 4 	//number of addressbits
)
(
	input wire [W-1:0] r_addr,
	output wire [B-1:0] r_data
);

//signal declaration
reg[B-1:0]array_reg[2**W-1:0];

//-- Inicializacion de la memoria. 
//-- Solo se dan valores a las 9 primeras posiciones
//-- El resto permanecera a 0
  initial begin
    array_reg[0] = 16'h180A; //LDI 10; ACC<-10 
    array_reg[1] = 16'h0800; //STO A ;A <- ACC; address
    array_reg[2] = 16'h1814; //LDI 20;  ACC<-20 
    array_reg[3] = 16'h0801; //STO B; B <- ACC 
    array_reg[4] = 16'h1000; //LD A; ACC<- A 
    array_reg[5] = 16'h2001; //ADD B; ACC<-ACC+B 
    array_reg[6] = 16'h3805; //SUBI 5; ACC<-ACC-5
    array_reg[7] = 16'h0802; //STO C; C<-ACC 
	array_reg[8] = 16'h1FF1; // LDI -15; ACC<- -15 
    array_reg[9] = 16'h3002; //	SUB C; ACC<-ACC-C
    array_reg[10] = 16'h0000; //
    array_reg[11] = 16'h0000; //
    array_reg[12] = 16'h0000; //
    array_reg[13] = 16'h0000; //
    array_reg[14] = 16'h0000; //
    array_reg[15] = 16'h0000; //
   end

//read operation
assign r_data = array_reg[r_addr];

endmodule
