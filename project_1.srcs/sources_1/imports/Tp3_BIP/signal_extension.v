module signal_extension
(
	input wire signed [10:0] in_se,
	output reg signed [15:0] out_se
);

reg [15:0] se_reg = 0;

always @(*)
begin 
	if (in_se[10])
		out_se=16'hF800|in_se;
	else
		out_se=16'h0000|in_se;
end

endmodule
