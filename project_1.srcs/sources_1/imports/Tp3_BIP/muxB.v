module muxB(
	input wire selectB,
	input wire signed[15:0] dm,
	input wire signed[15:0] se,
	output reg signed[15:0] muxB_out
);

always @(*)
begin
   case(selectB)
       0 : 
			muxB_out = dm; // data memory 
       1 : 
			muxB_out = se; // signal extension
   endcase
end

endmodule
