module instruction_decoder(
		input wire [4:0]opcode,
		output reg wrPC,
		output reg [1:0]selA,
		output reg selB,
		output reg wrACC,
		output reg op,
		output reg wrRAM,
		output reg rdRAM
);

always @(*)
	case (opcode)
		5'b00000: //HLT
			begin
				wrPC=0;
				selA=2'b00;
				selB=0;
				wrACC=0;
				op=0;
				wrRAM=0;
				rdRAM=0;
			end
		5'b00001: //STO
			begin
				wrPC=1;
				selA=2'b00;
				selB=0;
				wrACC=0;
				op=0;
				wrRAM=1;
				rdRAM=0;
			end
		5'b00010: //LD
			begin
				wrPC=1;
				selA=2'b00;
				selB=0;
				wrACC=1;
				op=0;
				wrRAM=0;
				rdRAM=1;
			end
		5'b00011: //LDI
			begin
				wrPC=1;
				selA=2'b01;
				selB=0;
				wrACC=1;
				op=0;
				wrRAM=0;
				rdRAM=0;
			end
		5'b00100: //ADD
			begin
				wrPC=1;
				selA=2'b10;
				selB=0;
				wrACC=1;
				op=1;
				wrRAM=0;
				rdRAM=1;
			end
		5'b00101: //ADDI
			begin
				wrPC=1;
				selA=2'b10;
				selB=1;
				wrACC=1;
				op=1;
				wrRAM=0;
				rdRAM=0;
			end
		5'b00110: //SUB
			begin
				wrPC=1;
				selA=2'b10;
				selB=0;
				wrACC=1;
				op=0;
				wrRAM=0;
				rdRAM=1;
			end
		5'b00111: //SUBI
			begin
				wrPC=1;
				selA=2'b10;
				selB=1;
				wrACC=1;
				op=0;
				wrRAM=0;
				rdRAM=0;
			end
		default:
			begin
				wrPC=0;
				selA=2'b00;
				selB=0;
				wrACC=0;
				op=0;
				wrRAM=0;
				rdRAM=0;
			end
	endcase	

endmodule