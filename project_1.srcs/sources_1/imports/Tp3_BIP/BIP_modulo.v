module BIP(
	input wire clk,
	input wire reset_pc,
	output wire [15:0]acc_out,
	output wire [15:0]pm_out,
	output wire [3:0]pm_addr,
	output wire [15:0]dm_out,
	output wire [10:0]dm_addr,
	output wire rd_enable,
	output wire wr_enable,
	output wire enable_pc,
	output wire [7:0]count
);

cpu cpu_1
(
	.clk(clk),
	.pm_out(pm_out),
	.dm_out(dm_out),
	.reset_pc(reset_pc),
	.pm_addr(pm_addr),
	.rd_enable(rd_enable),
	.wr_enable(wr_enable),
	.dm_addr(dm_addr),
	.acc_out(acc_out),
	.enable_pc(enable_pc)
 );
 
program_memory
#(
	.B(16),	//number of bits
	.W(4)	//number of addressbits
)
pm_1
(
	.r_addr(pm_addr),
	.r_data(pm_out)
);

data_memory
#(
	.B(16)	//number of bits
)
dm_1
(
	.addr(dm_addr),
	.wr_en(wr_enable),
	.rd_en(rd_enable),
	.in_data(acc_out),
	.out_data(dm_out)
);

contador_clk cuenta_ciclos_1
(
	.clk(clk), 
	.reset(reset_pc),
	.enable(enable_pc),
	.count(count)
);

endmodule
