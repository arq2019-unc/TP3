module acc_register
#(
	parameter B = 16	//number of bits
)
(
	input wire clk, enable,
	input wire signed[B-1:0] acc_in, 
	output reg signed[B-1:0] acc_out 
);

reg [B-1:0] acc_reg=0, acc_next=0;

always@(posedge clk)
begin
	if (enable)
		acc_reg<=acc_next;
end

always @(*)
begin
	acc_next=acc_in;
end

always @(*)
begin 
	acc_out=acc_reg;
end

endmodule

