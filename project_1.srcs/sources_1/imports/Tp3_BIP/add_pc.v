module add_pc(
	input wire[3:0] pc,
	output reg[3:0] pcadd
);

always @(*) 
begin
	pcadd=pc+1;
end
	
endmodule
