module datapath(
	input wire clk,
	input wire [1:0]selA,
	input wire selB,
	input wire wrACC,
	input wire op,
	input wire [10:0]operand_in,
	input wire [15:0]dm_out,
	//output wire [10:0]operand_out,
	output wire [15:0]acc_out //dm_in
);

wire [15:0]se_out;
wire [15:0]op_out;
wire [15:0]muxA_out;
wire [15:0]muxB_out;

signal_extension se
(
	.in_se(operand_in),
	.out_se(se_out)
);

muxA muxA_1
(
	.selectA(selA),
	.op(op_out),
	.se(se_out),
	.dm(dm_out),
	.muxA_out(muxA_out)
);

acc_register acc_reg_1
(
	.clk(clk), 
	.enable(wrACC),
	.acc_in(muxA_out), 
	.acc_out(acc_out)  
);
	 
 muxB muxB_1
(
	.selectB(selB),
	.dm(dm_out),
	.se(se_out),
	.muxB_out(muxB_out)
);

operation op_1
(
	.selectOp(op),
	.acc_out(acc_out),
	.muxB_out(muxB_out),
	.total_out(op_out)
);

endmodule