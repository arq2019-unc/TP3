module muxA(
	input wire[1:0] selectA,
	input wire signed[15:0] op,
	input wire signed[15:0] se,
	input wire signed[15:0] dm,
	output reg signed[15:0] muxA_out
);

always @(*)
begin
   case(selectA)
       0 : 
			muxA_out = dm; // data memory 
       1 : 
			muxA_out = se; // signal extension
       2 : 
			muxA_out = op; // operation output
       default: 
			muxA_out = 0; // default select
   endcase
end

endmodule
