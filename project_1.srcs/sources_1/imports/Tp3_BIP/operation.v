module operation(
	input wire selectOp,
	input wire signed [15:0] acc_out,
	input wire signed [15:0] muxB_out,
	output reg signed [15:0] total_out
);

always @(*)
begin
	case (selectOp)
		0:
			total_out = acc_out - muxB_out;
		1:
			total_out = acc_out + muxB_out;
	endcase
end

endmodule
