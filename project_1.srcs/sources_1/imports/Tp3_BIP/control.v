module control(
	output wire [1:0]selA,
	output wire selB,
	output wire wrACC,
	output wire op,
	output wire wrRAM,
	output wire rdRAM,
	output wire [10:0]operand_out,
 	output wire [3:0]pc_out,
	output wire enable_pc,
	input wire [15:0]pm_data,
	input wire clk,
	input wire reset_pc	
);
 
// wire enable_pc;
wire [3:0]pc_in; //d

instruction_decoder id
(
	.opcode(pm_data[15:11]),
	.wrPC(enable_pc),
	.selA(selA),
	.selB(selB),
	.wrACC(wrACC),
	.op(op),
	.wrRAM(wrRAM),
	.rdRAM(rdRAM)
);

pc_register pc_reg_1
(
	.clk(clk), 
	.enable(enable_pc),
	.pc_in(pc_in), //input
	.pc_out(pc_out), //output
	.reset(reset_pc)
);

add_pc add_pc_1
(
	.pc(pc_out), //input
	.pcadd(pc_in)//output
);

assign operand_out=pm_data[10:0];

endmodule
