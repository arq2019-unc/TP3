module contador_clk(
	input wire clk, 
	input wire reset,
	input wire enable,
    output reg [7:0] count
);

reg [7:0] count_reg=1, count_next=1;

always @(posedge clk,posedge reset)
begin
	if(reset) 
		count_reg <= 1;
	else 
		count_reg <= count_next;
end

always @(*)
begin
	if (enable)
		count_next = count_reg + 1;
end

always @(*)
begin
	count = count_reg;
end

endmodule
