module cpu(
	input wire clk,
	input wire [15:0]pm_out,
	input wire [15:0]dm_out,
	input wire reset_pc,
	output wire [3:0] pm_addr,
	output wire rd_enable,
	output wire	wr_enable,
	output wire	[10:0]dm_addr,
	output wire	[15:0]acc_out,
	output wire enable_pc
);

wire [1:0]selA;
wire selB;
wire wrACC;
wire op;
wire [10:0]operand_out;

control control_1
(
	.selA(selA),
	.selB(selB),
	.wrACC(wrACC),
	.op(op),
	.wrRAM(wr_enable),
	.rdRAM(rd_enable),
	.pm_data(pm_out),
	.operand_out(operand_out),
	.pc_out(pm_addr),
	.enable_pc(enable_pc),	
	.clk(clk),
	.reset_pc(reset_pc)
 );

datapath datapath_1
(
	.clk(clk),
	.selA(selA),
	.selB(selB),
	.wrACC(wrACC),
	.op(op),
	.operand_in(operand_out),
	.dm_out(dm_out),
	.acc_out(acc_out)
);

assign dm_addr = operand_out;

endmodule