module pc_register
#(
	parameter B = 4	// number of bits
)
(
	input wire clk, enable,reset,
	input wire [3:0] pc_in, // d
	output reg [3:0] pc_out // q
);

reg [3:0] pc_reg=0, pc_next=0; // la direccion inicial en 0

//body
always@(posedge clk)
begin
	if (~reset)
		begin
			pc_reg<=pc_next;
		end
	else
		begin
			pc_reg<=0;
		end
end

always @(*)
	if (enable)
		pc_next = pc_in;
		
always @(*)
	pc_out = pc_reg;	

endmodule
